package codegen;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BoundInfo {
	
	private int upperBound;
	private int lowerBound;

	public BoundInfo()
	{
		this(0, 0);
	}
	
	public BoundInfo(int bound)
	{
		this(bound, bound);
	}
	
	public BoundInfo (int lowerBound, int upperBound) throws IllegalArgumentException
	{
		if (lowerBound > upperBound)
		{
			throw new IllegalArgumentException("lowerBound must be leq to upperBound");
		}
		
		this.upperBound = upperBound;
		this.lowerBound = lowerBound;
	}
	
	public int getUpperBound()
	{
		return this.upperBound;
	}
	
	public void setUpperBound(int upperBound) throws IllegalArgumentException
	{
		if (upperBound < 1)
		{
			throw new IllegalArgumentException("to must be greater than 0");
		}
		
		this.upperBound = upperBound;
	}
	
	public int getLowerBound()
	{
		return this.lowerBound;
	}
	
	public void setLowerBound(int lowerBound) throws IllegalArgumentException
	{
		if (lowerBound < 1)
		{
			throw new IllegalArgumentException("from must be greater than 0");
		}
		
		this.lowerBound = lowerBound;
	}
	
	public void setBounds(int bound) throws IllegalArgumentException
	{
		if (bound < 1)
		{
			throw new IllegalArgumentException("slot must be greater than 0");
		}
		
		this.setLowerBound(bound);
		this.setUpperBound(bound);
	}
	
	public boolean validate()
	{
		return this.getLowerBound() <= this.getUpperBound();
	}
	
	public List<Integer> toList() throws IllegalStateException
	{
		if (! this.validate())
		{
			throw new IllegalStateException("lowerBound must be leq to upperBound");
		}
		
		return IntStream.range(this.getLowerBound(), this.getUpperBound() + 1)
				.boxed().collect(Collectors.toList());
	}
}
