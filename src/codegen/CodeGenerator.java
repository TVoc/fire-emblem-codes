package codegen;

import java.io.IOException;
import java.util.Iterator;
import java.util.Optional;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class CodeGenerator {

	public static void main(String[] args) {

		Options options = new Options();
		Option input = new Option("i", true, "Input code info file");
		Option slot = new Option("slot", true, "Character slot");
		Option from = new Option("from", true, "Character slot from");
		Option to = new Option("to", true, "Character slot to");
		Option allStats = new Option("allstats", false, "Generate default values for all stats (max values)");
		Option level = new Option("level", true, "Level");
		Option maxHP = new Option("maxhp", true, "Max HP");
		Option currHP = new Option("currenthp", true, "Current HP");
		Option str = new Option("str", true, "Strength");
		Option skl = new Option("skl", true, "Skill");
		Option spd = new Option("spd", true, "Speed");
		Option def = new Option("def", true, "Defence");
		Option res = new Option("res", true, "Resistance");
		Option luck = new Option("luck", true, "Luck");
		Option con = new Option("con", true, "Constitution bonus");
		Option move = new Option("move", true, "Move bonus");
		Option item1s = new Option("item1s", true, "Item 1");
		Option item1q = new Option("item1q", true, "Item 1 quantity");
		Option item2s = new Option("item2s", true, "Item 2");
		Option item2q = new Option("item2q", true, "Item 2 quantity");
		Option item3s = new Option("item3s", true, "Item 3");
		Option item3q = new Option("item3q", true, "Item 3 quantity");
		Option item4s = new Option("item4s", true, "Item 4");
		Option item4q = new Option("item4q", true, "Item 4 quantity");
		Option item5s = new Option("item5s", true, "Item 5");
		Option item5q = new Option("item5q", true, "Item 5 quantity");
		Option sword = new Option("sword", true, "Sword level");
		Option lance = new Option("lance", true, "Lance level");
		Option axe = new Option("axe", true, "Axe level");
		Option bow = new Option("bow", true, "Bow level");
		Option staff = new Option("staff", true, "Staff level");
		Option light = new Option("light", true, "Light level");
		Option dark = new Option("dark", true, "Dark level");
		Option anima = new Option("anima", true, "Anima level");
		
		options.addOption(input).addOption(slot).addOption(from).addOption(to).addOption(allStats)
			.addOption(level)
			.addOption(maxHP).addOption(currHP).addOption(str).addOption(skl)
			.addOption(spd).addOption(def).addOption(res).addOption(luck)
			.addOption(con).addOption(move).addOption(item1s).addOption(item1q)
			.addOption(item2s).addOption(item2q).addOption(item3s).addOption(item3q)
			.addOption(item4s).addOption(item4q).addOption(item5s).addOption(item5q)
			.addOption(sword).addOption(lance).addOption(axe).addOption(bow)
			.addOption(staff).addOption(light).addOption(dark).addOption(anima);
		
		try {
			DefaultParser parser = new DefaultParser();
			
			CommandLine cmd = parser.parse(options, args);
			
			Optional<CodeInfo> codeInfo = Optional.empty();
			Optional<BoundInfo> boundInfo = Optional.empty();
			
			if (! cmd.hasOption("slot") && ! (cmd.hasOption("from") && cmd.hasOption("to")))
			{
				throw new IllegalArgumentException("You must provide either -slot or -from and -to");
			}
			
			if (cmd.hasOption("slot"))
			{
				boundInfo = Optional.of(new BoundInfo(Integer.parseInt(cmd.getOptionValue("slot"))));
			}
			
			if (cmd.hasOption("from"))
			{
				boundInfo = Optional.of(new BoundInfo(Integer.parseInt(cmd.getOptionValue("from"))
						, Integer.parseInt(cmd.getOptionValue("to"))));
			}
			
			codeInfo = Optional.of(new GameFileReader().parse(cmd.getOptionValue("i")));
			
			for (int i : boundInfo.get().toList())
			{
				int diff = codeInfo.get().getDiff() * (i - 1);
				
				Iterator<Option> it = cmd.iterator();
				
				while (it.hasNext())
				{
					Option opt = it.next();
					Optional<Integer> base = codeInfo.get().determineBase(opt.getOpt());
					
					if (base.isPresent())
					{
						if (opt.getOpt().equals("allstats"))
						{
							for (String command : codeInfo.get().getStatsCmds())
							{
								System.out.print(String.format("%X", diff + base.get()
										+ codeInfo.get().getCmdCode(command), 16));
								System.out.print(String.format(" %04X", codeInfo.get().determineMaxStat(command) & 0xFFFF));
								System.out.print(System.lineSeparator());
							}
						}
						
						else
						{
							System.out.print(String.format("%X", diff + base.get()
								+ codeInfo.get().getCmdCode(opt.getOpt())));
							if (codeInfo.get().hasCmdCode(opt.getValue()))
							{
								System.out.print(String.format(" %04X", codeInfo.get().getCmdCode(opt.getValue()) & 0xFFFF));
							}
							else
							{
								System.out.print(String.format(" %04X", Integer.parseInt(opt.getValue()) & 0xFFFF));
							}
							System.out.print(System.lineSeparator());
						}

					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

}
