package codegen;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class CodeInfo {
	
	public CodeInfo(int statBase, int itemBase, int weaponBase, int diff, Map<String, Integer> cmdMap)
	{
		this.statBase = statBase;
		this.itemBase = itemBase;
		this.weaponBase = weaponBase;
		
		this.diff = diff;
		
		this.cmdMap = cmdMap;
	}
	
	private final int statBase;
	private final int itemBase;
	private final int weaponBase;
	
	private final int diff;
	
	private final Map<String, Integer> cmdMap;

	public int getStatBase()
	{
		return this.statBase;
	}

	public int getItemBase()
	{
		return this.itemBase;
	}

	public int getWeaponBase()
	{
		return this.weaponBase;
	}
	
	public int getDiff()
	{
		return this.diff;
	}

	private Map<String, Integer> internalGetCmdMap()
	{
		return this.cmdMap;
	}
	
	public Map<String, Integer> getCmdMap()
	{
		return Collections.unmodifiableMap(this.internalGetCmdMap());
	}
	
	public Integer getCmdCode(String key)
	{
		return this.internalGetCmdMap().get(key);
	}
	
	public boolean hasCmdCode(String key)
	{
		return this.internalGetCmdMap().containsKey(key);
	}
	
	public boolean representsStatCmd(String cmd)
	{
		switch (cmd)
		{
			case "allstats" : return true;
			case "level" : return true;
			case "maxhp" : return true;
			case "currenthp" : return true;
			case "str" : return true;
			case "skl" : return true;
			case "spd" : return true;
			case "def" : return true;
			case "res" : return true;
			case "luck" : return true;
			case "con" : return true;
			case "move" : return true;
			default : return false;
		}
	}
	
	public boolean representsItemCmd(String cmd)
	{
		switch (cmd)
		{
			case "item1s" : return true;
			case "item1q" : return true;
			case "item2s" : return true;
			case "item2q" : return true;
			case "item3s" : return true;
			case "item3q" : return true;
			case "item4s" : return true;
			case "item4q" : return true;
			case "item5s" : return true;
			case "item5q" : return true;
			default : return false;
		}
	}
	
	public boolean representsWepCmd(String cmd)
	{
		switch (cmd)
		{
			case "sword" : return true;
			case "lance" : return true;
			case "axe" : return true;
			case "bow" : return true;
			case "staff" : return true;
			case "anima" : return true;
			case "light" : return true;
			case "dark" : return true;
			default : return false;
		}
	}
	
	public Optional<Integer> determineBase(String cmd)
	{
		if (this.representsStatCmd(cmd))
		{
			return Optional.of(this.getStatBase());
		}
		if (this.representsItemCmd(cmd))
		{
			return Optional.of(this.getItemBase());
		}
		if (this.representsWepCmd(cmd))
		{
			return Optional.of(this.getWeaponBase());
		}
		
		return Optional.empty();
	}
	
	public List<String> getStatsCmds()
	{
		return Arrays.asList(
				new String[] 
				{
					"maxhp",
					"currenthp",
					"str",
					"skl",
					"spd",
					"def",
					"res",
					"luck",
				}
							);
	}
	
	public int determineMaxStat(String cmd) throws IllegalArgumentException
	{
		switch (cmd)
		{
			case "maxhp" : return 60;
			case "currenthp" : return 60;
			case "str" : return 30;
			case "skl" : return 30;
			case "spd" : return 30;
			case "def" : return 30;
			case "res" : return 30;
			case "luck" : return 30;
			default : throw new IllegalArgumentException(cmd + " is not a stat command");
		}
	}
}
