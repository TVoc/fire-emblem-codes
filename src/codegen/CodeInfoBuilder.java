package codegen;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CodeInfoBuilder {
	
	private int statBase = 0;
	private int itemBase = 0;
	private int weaponBase = 0;
	
	private int diff = 0;

	public int getStatBase()
	{
		return this.statBase;
	}
	
	public CodeInfoBuilder setStatBase(int statBase)
	{
		this.statBase = statBase;
		
		return this;
	}
	
	public int getItemBase()
	{
		return this.itemBase;
	}
	
	public CodeInfoBuilder setItemBase(int itemBase)
	{
		this.itemBase = itemBase;
		
		return this;
	}
	
	public int getWeaponBase()
	{
		return this.weaponBase;
	}
	
	public CodeInfoBuilder setWeaponBase(int weaponBase)
	{
		this.weaponBase = weaponBase;
		
		return this;
	}
	
	public int getDiff()
	{
		return this.diff;
	}
	
	public CodeInfoBuilder setDiff(int diff)
	{
		this.diff = diff;
		
		return this;
	}
	
	private Map<String, Integer> cmdMap = new HashMap<String, Integer>();
	
	private Map<String, Integer> internalGetCmdMap()
	{
		return this.cmdMap;
	}
	
	public Map<String, Integer> getCmdMap()
	{
		return Collections.unmodifiableMap(this.internalGetCmdMap());
	}
	
	public Integer getCmdCode(String key)
	{
		return this.internalGetCmdMap().get(key);
	}
	
	public boolean hasCmdCode(String key)
	{
		return this.internalGetCmdMap().containsKey(key);
	}
	
	public CodeInfoBuilder setCmdCode(String key, int code)
	{
		this.internalGetCmdMap().put(key, code);
		
		return this;
	}
	
	public CodeInfo build()
	{
		return new CodeInfo(this.getStatBase(), this.getItemBase()
				, this.getWeaponBase(), this.getDiff(), this.internalGetCmdMap());
	}
}
