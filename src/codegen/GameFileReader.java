package codegen;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class GameFileReader {

	public CodeInfo parse(String path) throws IOException
	{
		CodeInfoBuilder builder = new CodeInfoBuilder();
		
		BufferedReader reader = new BufferedReader(new FileReader(path));
		
		String line = reader.readLine();
		
		while (line != null)
		{
			String[] lineParts = line.split(": ");
			
			if (lineParts.length > 1)
			{
				if (lineParts[0].equalsIgnoreCase("stat base"))
				{
					builder.setStatBase(Integer.parseInt(lineParts[1], 16));
				}
				else if (lineParts[0].equalsIgnoreCase("item base"))
				{
					builder.setItemBase(Integer.parseInt(lineParts[1], 16));
				}
				else if (lineParts[0].equalsIgnoreCase("weapon base"))
				{
					builder.setWeaponBase(Integer.parseInt(lineParts[1], 16));
				}
				else if (lineParts[0].equalsIgnoreCase("diff"))
				{
					builder.setDiff(Integer.parseInt(lineParts[1], 16));
				}
				else
				{
					builder.setCmdCode(lineParts[0].replaceAll(" ", ""), Integer.parseInt(lineParts[1], 16));
				}
				
				line = reader.readLine();
				continue;
			}
			
			lineParts = line.split(" = ");
			
			if (lineParts.length > 1)
			{
				if (GameFileReader.representsInteger(lineParts[0], 16))
				{
					builder.setCmdCode(lineParts[1].toLowerCase().replaceAll(" ", ""), Integer.parseInt(lineParts[0], 16));
				}
				else
				{
					builder.setCmdCode(lineParts[0].toLowerCase().replaceAll(" ", ""), Integer.parseInt(lineParts[1], 16));
				}
			}
			
			line = reader.readLine();
		}
		
		reader.close();
		
		return builder.build();
	}
	
	public static boolean representsInteger(String in, int radix)
	{
		try
		{
			Integer.parseInt(in, radix);
			return true;
			
		} catch (NumberFormatException e)
		{
			return false;
		}
	}
}
